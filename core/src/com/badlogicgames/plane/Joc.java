package com.badlogicgames.plane;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;

/**
 * Created by damviod on 22/04/16.
 */
public class Joc extends Game {
    Screen[] screens;
    public GameState gameState;

    @Override
    public void create() {
        gameState = GameState.Start;
        screens = new Screen[2];
        screens[0] = new PlaneGame().PlaneGame(this);
        screens[1] = new Menu().Menu(this);
        setScreen(screens[1]);
    }

    public static enum GameState {
        Start, Running, GameOver
    }
}
