/*
KTHXBYE License Version 2.0!

 * Do NOT slap your corporation's license header anywhere, it's the law :D
 * You can totally do this:
   - fork it, spoon it, knife it
   - redistribute it, in source, binary, pizza or lasagne form
   - modify it, convert it, remix it, blend it
 * ALWAYS retain the CREDITS file, it credits the sources for all art, namely
   - The AWESOME Kenney aka Asset Jesus (http://www.kenney.nl)
   - "Bacterial Love" by RoleMusic (http://freemusicarchive.org/music/Rolemusic/Pop_Singles_Compilation_2014/01_rolemusic_-_bacterial_love)
 * ALWAYS retain this LICENSE file and any related material such as license headers.
 * IF USED FOR COMMERCIAL PURPOSES (including training material, talks, etc.) YOU MUST:
   - Take a photo of you wearing a pink hat, standing on one leg, holding a turtle
     - Turtle may be substituted by chicken, polar bear, or great white shark
   - Post the photo to Twitter along with the message "Am I pretty @badlogicgames?"

 If you violate this license, Karma will be a bitch (and i'll be petty on Twitter)!

Kthxbye, <3 Mario (Zechner, Copyright 2014-2234)
*/

package com.badlogicgames.plane;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import java.lang.reflect.Constructor;
import java.security.Key;

public class PlaneGame implements Screen {
	private static final float PLANE_JUMP_IMPULSE = 350;
	private static final float GRAVITY = -20;
	private static final float PLANE_VELOCITY_X = 200;
	private static final float PLANE_START_Y = 240;
	private static final float PLANE_START_X = 50;

	Plane plane;

	ShapeRenderer shapeRenderer;
	SpriteBatch batch;
	OrthographicCamera camera;
	OrthographicCamera uiCamera;
	Texture background;
	TextureRegion ground;
	float groundOffsetX = 0;
	TextureRegion ceiling;
	TextureRegion rock;
	TextureRegion rockDown;
	TextureRegion ready;
	TextureRegion gameOver;
	BitmapFont font;
	Joc game;
	float planeStateTime = 0;
	Vector2 gravity = new Vector2();
	Array<Rock> rocks = new Array<Rock>();

	//PowerUP
	int powerup = 3;
	float timePWD = 0;
	Boolean isPowered = false;


	int score = 0;
	Rectangle rect2 = new Rectangle();
	
	Music music;
	Sound explode;

	public PlaneGame PlaneGame (Joc joc) {
		plane = new Plane().Plane();
		game = joc;
		shapeRenderer = new ShapeRenderer();
		batch = new SpriteBatch();
		camera = new OrthographicCamera();
		camera.setToOrtho(false, 800, 480);
		uiCamera = new OrthographicCamera();
		uiCamera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		uiCamera.update();
		
		font = new BitmapFont(Gdx.files.internal("arial.fnt"));
		
		background = new Texture("background.png");	
		ground = new TextureRegion(new Texture("ground.png"));
		ceiling = new TextureRegion(ground);
		ceiling.flip(true, true);
		
		rock = new TextureRegion(new Texture("rock.png"));
		rockDown = new TextureRegion(rock);
		rockDown.flip(false, true);
		ready = new TextureRegion(new Texture("ready.png"));
		gameOver = new TextureRegion(new Texture("gameover.png"));
		
		music = Gdx.audio.newMusic(Gdx.files.internal("music.mp3"));
		music.setLooping(true);
		music.play();
		
		explode = Gdx.audio.newSound(Gdx.files.internal("explode.wav"));
		
		resetWorld();
		return this;
	}
	
	private void resetWorld() {
		score = 0;
		groundOffsetX = 0;
		plane.planePosition.set(PLANE_START_X, PLANE_START_Y);
		plane.planeVelocity.set(0, 0);
		gravity.set(0, GRAVITY);
		camera.position.x = 400;
		
		rocks.clear();
		for(int i = 0; i < 5; i++) {
			boolean isDown = MathUtils.randomBoolean();
			rocks.add(new Rock(700 + i * 200, isDown?480-rock.getRegionHeight(): 0, isDown? rockDown: rock));
		}
	}
	
	private void updateWorld(float dt) {
		planeStateTime += dt;
		
		if(Gdx.input.justTouched()) {
			if(game.gameState == Joc.GameState.Start) {
				game.gameState = Joc.GameState.Running;
			}
			if(game.gameState == Joc.GameState.Running) {
				plane.SetVelocity(PLANE_VELOCITY_X, PLANE_JUMP_IMPULSE);
			}
			if(game.gameState == Joc.GameState.GameOver) {
				game.gameState = Joc.GameState.Start;
				resetWorld();
				game.setScreen(game.screens[1]);
			}
		}

		if (Gdx.input.isKeyPressed(Input.Keys.SPACE) && powerup>0 && !isPowered)
		{
			powerup--;
			isPowered = true;
		}

		if (isPowered && timePWD <= 5)
			timePWD += dt;

		if (timePWD > 5)
			isPowered = false;

			
		if(game.gameState != Joc.GameState.Start) plane.addGravity(gravity);

		plane.mulAddPos(plane.planeVelocity, dt);

		camera.position.x = plane.planePosition.x + 350;
		if(camera.position.x - groundOffsetX > ground.getRegionWidth() + 400) {
			groundOffsetX += ground.getRegionWidth();
		}

		plane.rect.set(plane.planePosition.x + 20, plane.planePosition.y, plane.animation.getKeyFrames()[0].getRegionWidth() - 20, plane.animation.getKeyFrames()[0].getRegionHeight());
		for(Rock r: rocks) {
			if(camera.position.x - r.position.x > 400 + r.image.getRegionWidth()) {
				boolean isDown = MathUtils.randomBoolean();
				r.position.x += 5 * 200;
				r.position.y = isDown?480-rock.getRegionHeight(): 0;
				r.image = isDown? rockDown: rock;
				r.counted = false;
			}
			rect2.set(r.position.x + (r.image.getRegionWidth() - 30) / 2 + 20, r.position.y, 20, r.image.getRegionHeight() - 10);
			if(plane.rect.overlaps(rect2) && !isPowered) {
				if(game.gameState != Joc.GameState.GameOver) explode.play();
				game.gameState = Joc.GameState.GameOver;
				plane.planeVelocity.x = 0;
			}
			if(r.position.x < plane.planePosition.x && !r.counted) {
				score++;
				r.counted = true;
			}
		}
		
		if(plane.planePosition.y < ground.getRegionHeight() - 20 || plane.planePosition.y + plane.animation.getKeyFrames()[0].getRegionHeight() > 480 - ground.getRegionHeight() + 20) {
			if(game.gameState != Joc.GameState.GameOver) explode.play();
			game.gameState = Joc.GameState.GameOver;
			plane.planeVelocity.x = 0;
		}		
	}
	
	private void drawWorld() {
		camera.update();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		batch.draw(background, camera.position.x - background.getWidth() / 2, 0);
		for(Rock rock: rocks) {
			batch.draw(rock.image, rock.position.x, rock.position.y);
		}
		batch.draw(ground, groundOffsetX, 0);
		batch.draw(ground, groundOffsetX + ground.getRegionWidth(), 0);
		batch.draw(ceiling, groundOffsetX, 480 - ceiling.getRegionHeight());
		batch.draw(ceiling, groundOffsetX + ceiling.getRegionWidth(), 480 - ceiling.getRegionHeight());
		batch.draw(plane.animation.getKeyFrame(planeStateTime), plane.planePosition.x, plane.planePosition.y);
		batch.end();
		
		batch.setProjectionMatrix(uiCamera.combined);
		batch.begin();		
		if(game.gameState == Joc.GameState.Start) {
			batch.draw(ready, Gdx.graphics.getWidth() / 2 - ready.getRegionWidth() / 2, Gdx.graphics.getHeight() / 2 - ready.getRegionHeight() / 2);
		}
		if(game.gameState == Joc.GameState.GameOver) {
			batch.draw(gameOver, Gdx.graphics.getWidth() / 2 - gameOver.getRegionWidth() / 2, Gdx.graphics.getHeight() / 2 - gameOver.getRegionHeight() / 2);
		}
		if(game.gameState == Joc.GameState.GameOver || game.gameState == Joc.GameState.Running) {
			font.draw(batch, "" + score, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 60);
		}
		batch.end();
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		updateWorld(delta);
		drawWorld();
	}

	@Override
	public void resize(int width, int height) {

	}

	@Override
	public void show() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {

	}

	static class Rock {
		Vector2 position = new Vector2();
		TextureRegion image;
		boolean counted;
		
		public Rock(float x, float y, TextureRegion image) {
			this.position.x = x;
			this.position.y = y;
			this.image = image;
		}
	}
}
