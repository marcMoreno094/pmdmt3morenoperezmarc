package com.badlogicgames.plane;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import java.util.Random;

/**
 * Created by damviod on 22/04/16.
 */
public class Menu implements Screen {
    public static final float PLANE_JUMP_IMPULSE = 350;
    public static final float GRAVITY = -20;
    public static final float PLANE_VELOCITY_X = 200;
    public static final float PLANE_START_Y = 240;
    public static final float PLANE_START_X = 50;

    ShapeRenderer shapeRenderer;
    SpriteBatch batch;
    OrthographicCamera camera;
    OrthographicCamera uiCamera;
    Texture background;
    TextureRegion ground;
    float groundOffsetX = 0;
    TextureRegion ceiling;
    TextureRegion rock;
    TextureRegion rockDown;
    Animation plane;
    TextureRegion ready;
    TextureRegion gameOver;
    BitmapFont font;
    Joc game;
    Vector2 planePosition = new Vector2();
    Vector2 planeVelocity = new Vector2();
    float planeStateTime = 0;
    Vector2 gravity = new Vector2();
    //Array<Rock> rocks = new Array<Rock>();

    int score = 0;
    Rectangle rect1 = new Rectangle();
    Rectangle rect2 = new Rectangle();

    Music music;
    Sound explode;

    public Menu Menu(Joc joc){
        game = joc;

        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);
        uiCamera = new OrthographicCamera();
        uiCamera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        uiCamera.update();

        font = new BitmapFont(Gdx.files.internal("arial.fnt"));

        background = new Texture("background.png");
        ground = new TextureRegion(new Texture("ground.png"));
        ceiling = new TextureRegion(ground);
        ceiling.flip(true, true);

        rock = new TextureRegion(new Texture("rock.png"));
        rockDown = new TextureRegion(rock);
        rockDown.flip(false, true);

        Texture frame1 = new Texture("plane1.png");
        frame1.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        Texture frame2 = new Texture("plane2.png");
        Texture frame3 = new Texture("plane3.png");

        ready = new TextureRegion(new Texture("ready.png"));
        gameOver = new TextureRegion(new Texture("gameover.png"));

        plane = new Animation(0.05f, new TextureRegion(frame1), new TextureRegion(frame2), new TextureRegion(frame3), new TextureRegion(frame2));
        plane.setPlayMode(Animation.PlayMode.LOOP);

        resetWorld();
        return this;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(1, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        drawWorld();

        if(Gdx.input.justTouched()) {
            if (game.gameState == Joc.GameState.Start) {
                game.gameState = Joc.GameState.Running;
            }
        }

        if (game.gameState == Joc.GameState.Running)
            game.setScreen(game.screens[0]);
    }

    private void resetWorld() {
        score = 0;
        groundOffsetX = 0;
        planePosition.set(PLANE_START_X, PLANE_START_Y);
        planeVelocity.set(0, 0);
        gravity.set(0, GRAVITY);
        camera.position.x = 400;

        //rocks.clear();
        /*for(int i = 0; i < 5; i++) {
            boolean isDown = MathUtils.randomBoolean();
            rocks.add(new Rock(700 + i * 200, isDown?480-rock.getRegionHeight(): 0, isDown? rockDown: rock));
        }*/
    }

    private void drawWorld() {
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        batch.begin();
        batch.draw(background, camera.position.x - background.getWidth() / 2, 0);
        /*for(Rock rock: rocks) {
            batch.draw(rock.image, rock.position.x, rock.position.y);
        }*/
        batch.draw(ground, groundOffsetX, 0);
        batch.draw(ground, groundOffsetX + ground.getRegionWidth(), 0);
        batch.draw(ceiling, groundOffsetX, 480 - ceiling.getRegionHeight());
        batch.draw(ceiling, groundOffsetX + ceiling.getRegionWidth(), 480 - ceiling.getRegionHeight());
        batch.draw(plane.getKeyFrame(planeStateTime), planePosition.x + new Random().nextFloat()*5, planePosition.y + new Random().nextFloat()*5);
        batch.end();

        batch.setProjectionMatrix(uiCamera.combined);
        batch.begin();
        if(game.gameState == Joc.GameState.Start) {
            batch.draw(ready, Gdx.graphics.getWidth() / 2 - ready.getRegionWidth() / 2, Gdx.graphics.getHeight() / 2 - ready.getRegionHeight() / 2);
        }
        if(game.gameState == Joc.GameState.GameOver) {
            batch.draw(gameOver, Gdx.graphics.getWidth() / 2 - gameOver.getRegionWidth() / 2, Gdx.graphics.getHeight() / 2 - gameOver.getRegionHeight() / 2);
        }
        if(game.gameState == Joc.GameState.GameOver || game.gameState == Joc.GameState.Running) {
            font.draw(batch, "" + score, Gdx.graphics.getWidth() / 2, Gdx.graphics.getHeight() - 60);
        }
        batch.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void show() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
