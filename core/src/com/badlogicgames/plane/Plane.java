package com.badlogicgames.plane;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by damviod on 22/04/16.
 */
public class Plane {
    Texture frame1 = new Texture("plane1.png");
    Texture frame2 = new Texture("plane2.png");
    Texture frame3 = new Texture("plane3.png");

    Animation animation;
    Vector2 planePosition = new Vector2();
    Vector2 planeVelocity = new Vector2();

    Rectangle rect = new Rectangle();

    public Plane Plane(){

        frame1.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        animation = new Animation(0.05f, new TextureRegion(frame1), new TextureRegion(frame2), new TextureRegion(frame3), new TextureRegion(frame2));
        animation.setPlayMode(PlayMode.LOOP);

        return this;
    }

    public void SetVelocity(float x, float y){
        planeVelocity.set(x, y);
    }

    public void addGravity(Vector2 g){
        planeVelocity.add(g);
    }

    public void mulAddPos(Vector2 v, float f){
        planePosition.mulAdd(v,f);
    }
}
